/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cameraopen;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author alexd
 */
public class Video {
    VideoCapture cap = new VideoCapture(0);

    if (!cap.isOpened()) {
        System.out.println("Camera Error");
    } else {
        System.out.println("Camera OK?");
        cap.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, width);
        cap.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, height);
    }
    try {
        Thread.sleep(1000);
    } catch (InterruptedException ex) {
    }
    cap.read(mat);
    System.out.println("width, height = "+mat.cols()+", "+mat.rows());
}
