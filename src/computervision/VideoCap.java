
package computervision;

import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Size;
//import org.opencv.highgui.VideoCapture;
import org.opencv.videoio.VideoCapture;
import org.opencv.imgproc.Imgproc;

public class VideoCap {
    
    Mat firstFrame = new Mat();
    Mat referenceFrame = new Mat();
    Mat frameCompareble = new Mat();
    Mat frameDelta = new Mat();
    Mat thresh = new Mat();
    List<MatOfPoint> cnts = new ArrayList<MatOfPoint>();
    
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    VideoCapture camera;
    Mat2Image mat2Img = new Mat2Image();

    VideoCap(){
        camera = new VideoCapture();
        camera.open(0);
        
                camera.read(mat2Img.frame);
		//convert to grayscale and set the first frame
		Imgproc.cvtColor(mat2Img.frame, referenceFrame, Imgproc.COLOR_BGR2GRAY);
		Imgproc.GaussianBlur(referenceFrame, referenceFrame, new Size(21, 21), 0);
    } 
 
    BufferedImage getOneFrame() {
        
			//convert to grayscale
			Imgproc.cvtColor(mat2Img.frame, frameCompareble, Imgproc.COLOR_BGR2GRAY);
			Imgproc.GaussianBlur(frameCompareble, frameCompareble, new Size(21, 21), 0);
                        
			//compute difference between first frame and current frame
			Core.absdiff(referenceFrame, frameCompareble, frameDelta);
			Imgproc.threshold(frameDelta, thresh, 25, 255, Imgproc.THRESH_BINARY);
        
                        Imgproc.dilate(thresh, thresh, new Mat(), new Point(-1, -1), 2);
			Imgproc.findContours(thresh, cnts, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
                        
			for(int i=0; i < cnts.size(); i++) {
				if(Imgproc.contourArea(cnts.get(i)) < 180900) {
					continue;
				}
                                referenceFrame = frameCompareble;
                                HoraAgora();
				System.out.println("Motion detected !!!"+Imgproc.contourArea(cnts.get(i)));
				
			}

        camera.read(mat2Img.frame);
        return mat2Img.getImage(mat2Img.frame);
    }
    
    public void HoraAgora() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println( sdf.format(cal.getTime()) );
    }
}
