
package proprias.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import proprias.entidade.Movimento;
import proprias.util.FabricaConexao;

/**
 *
 * @author Alex
 */
public class MovimentoDao {
  private String sql;
  public boolean salvar(Movimento movimento){
          Connection conexao = FabricaConexao.getConexao();
          PreparedStatement stmt;
      try {
          sql = "INSERT INTO movimento (pos_x, pos_y) "
                  + " VALUES (?,?)";
          stmt =  conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
          stmt.setInt(1, movimento.getPos_x());
          stmt.setInt(2, movimento.getPos_y());
          stmt.execute();
          System.out.println("Dados Inseridos!");
          ResultSet chaves = stmt.getGeneratedKeys();
          FabricaConexao.fecharConexao();
          return true;
      } catch (SQLException ex) {
          Logger.getLogger(MovimentoDao.class.getName()).log(Level.SEVERE, "", ex);
      }

      FabricaConexao.fecharConexao();
      return false;
  }

  public List<Movimento> pesquisarPorRange(int id_inicio, int id_final, int limite_x, int limite_y) {
      List<Movimento> lista = new LinkedList<>();
      sql = "SELECT id, pos_x, pos_y\n" +
            " FROM movimento p "
           + "WHERE p.id > ? "
              + "AND p.id < ? "
              + "AND pos_x < ? "
              + "AND pos_y < ? "
              + "order by pos_x, pos_x";
      try(Connection conexao = FabricaConexao.getConexao();
            PreparedStatement stmt = conexao.prepareStatement(sql)){

          System.out.println("Conex�o aberta!");
          stmt.setInt(1, id_inicio);
          stmt.setInt(2, id_final);
          stmt.setInt(3, limite_x);
          stmt.setInt(4, limite_y);
          ResultSet rs = stmt.executeQuery();
          while(rs.next()){
            int id = rs.getInt("id");
            int pos_x = rs.getInt("pos_x");
            int pos_y = rs.getInt("pos_y");
            lista.add(new Movimento(id, pos_x, pos_y));
          }

          FabricaConexao.fecharConexao();
          return lista;

      }catch(SQLException e){ 
        System.out.println("Exce��o no m�todo pesquisarPorRange "+e.getMessage());
      }catch(Exception e){  
        System.out.println("Exce��o no c�digo pesquisarPorRange! "+e);
      }
      FabricaConexao.fecharConexao();
      return null;

  }

  public List<Movimento> pesquisarMaiorQue(int id_inicio, int limite_x, int limite_y) {
      List<Movimento> lista = new LinkedList<>();
      sql = "SELECT id, pos_x, pos_y\n" +
            " FROM movimento p "
           + "WHERE p.id > ? "
              + "AND pos_x < ? "
              + "AND pos_y < ? "
              + "order by pos_x, pos_x";
      try(Connection conexao = FabricaConexao.getConexao();
            PreparedStatement stmt = conexao.prepareStatement(sql)){

          System.out.println("Conex�o aberta!");
          stmt.setInt(1, id_inicio);
          stmt.setInt(2, limite_x);
          stmt.setInt(3, limite_y);
          ResultSet rs = stmt.executeQuery();
          while(rs.next()){
            int id = rs.getInt("id");
            int pos_x = rs.getInt("pos_x");
            int pos_y = rs.getInt("pos_y");
            lista.add(new Movimento(id, pos_x, pos_y));
          }

          FabricaConexao.fecharConexao();
          return lista;

      }catch(SQLException e){ 
        System.out.println("Exce��o no m�todo pesquisarMaiorQue "+e.getMessage());
      }catch(Exception e){  
        System.out.println("Exce��o no c�digo pesquisarMaiorQue! "+e);
      }
      FabricaConexao.fecharConexao();
      return null;

  }
  
}
