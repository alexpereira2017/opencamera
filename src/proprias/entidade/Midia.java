/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proprias.entidade;

import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import org.opencv.core.Mat;
//import static proprias.entidade.AbstrataMidia.Mat2bufferedImage;
import proprias.util.MapaDeCalor;

public interface Midia {
  public void start();
  public void setLinkFonte(String n);
  public void frameGenerator();
  public Mat getImagem();
  public Mat getImagem(MapaDeCalor m);
//  public BufferedImage bufferedImage();
  public ImageIcon getImagemParaExibir();
  public void exibir(Tela tela);
}
