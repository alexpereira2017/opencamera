
package proprias.entidade;

public class Configuracao {

  @Override
  public String toString() {
    return "Configuracao{" + "tamanhoTelaX=" + tamanhoTelaX + ", tamanhoTelaY=" + tamanhoTelaY + ", celulaAltura=" + celulaAltura + ", celulaLargura=" + celulaLargura + ", resolucaoNumeroLinhas=" + resolucaoNumeroLinhas + ", resolucaoNumeroColunas=" + resolucaoNumeroColunas + ", alpha=" + alpha + ", nomeTela=" + nomeTela + '}';
  }
  private int tamanhoTelaX, tamanhoTelaY, 
              celulaAltura, celulaLargura;
  private int resolucaoNumeroLinhas;
  private int resolucaoNumeroColunas;
  private Float alpha;
  private String pathFonteVideo, nomeFonteVideo;

  public String getPathFonteVideo() {
    return pathFonteVideo;
  }

  public void setPathFonteVideo(String pathFonteVideo) {
    this.pathFonteVideo = pathFonteVideo;
  }

  public String getNomeFonteVideo() {
    return nomeFonteVideo;
  }

  public void setNomeFonteVideo(String nomeFonteVideo) {
    this.nomeFonteVideo = nomeFonteVideo;
  }

  public Float getAlpha() {
    return alpha;
  }

  public void setAlpha(Float alpha) {
    this.alpha = alpha;
  }

  public int getResolucaoNumeroLinhas() {
    return resolucaoNumeroLinhas;
  }

  public void setResolucaoNumeroLinhas(int resolucaoNumeroLinhas) {
    this.resolucaoNumeroLinhas = resolucaoNumeroLinhas;
  }

  public int getResolucaoNumeroColunas() {
    return resolucaoNumeroColunas;
  }

  public void setResolucaoNumeroColunas(int resolucaoNumeroColunas) {
    this.resolucaoNumeroColunas = resolucaoNumeroColunas;
  }
  
  private String nomeTela;

  public int getCelulaAltura() {
    return celulaAltura;
  }

  public void setCelulaAltura(int celulaAltura) {
    this.celulaAltura = celulaAltura;
  }

  public int getCelulaLargura() {
    return celulaLargura;
  }

  public void setCelulaLargura(int celulaLargura) {
    this.celulaLargura = celulaLargura;
  }

  public String getNomeTela() {
    return nomeTela;
  }

  public void setNomeTela(String nomeTela) {
    this.nomeTela = nomeTela;
  }

  public int getTamanhoTelaX() {
    return tamanhoTelaX;
  }

  public void setTamanhoTelaX(int tamanhoTelaX) {
    this.tamanhoTelaX = tamanhoTelaX;
  }

  public int getTamanhoTelaY() {
    return tamanhoTelaY;
  }

  public void setTamanhoTelaY(int tamanhoTelaY) {
    this.tamanhoTelaY = tamanhoTelaY;
  }
  
}
