
package proprias.entidade;

import proprias.util.DeteccaoMovimentoOriginal;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import proprias.util.MapaDeCalor;
import org.opencv.utils.Converters;

/**
 *
 * @author alexd
 */
public class WebCam extends AbstrataMidia {
  private VideoCapture camera;
  private MapaDeCalor mapa;

  @Override
  public void start() {
//    camera = new VideoCapture(1);
	camera = new VideoCapture(0);
  }

  @Override
  public void setLinkFonte(String n) {}

  @Override
  public void frameGenerator() {
    camera.read(frame);
  }

  @Override
  public Mat getImagem() {
    imag = frame.clone();
    return imag;
  }
  
  public Mat getFrame() {
    return frame;
  }

  @Override
  public Mat getImagem(MapaDeCalor m) {
    imag = m.montar(frame.clone());
    return imag;
  }

  @Override
  public void exibir(Tela tela) {
    start();

//    List<Integer> listaPosicoes = new ArrayList<Integer>();

//    DeteccaoMovimentoOriginal det = new DeteccaoMovimentoOriginal();

    long startTime = 0;

    while(startTime < 500 ) {
//    while(true) {
      startTime++;
 
      frameGenerator();

      getImagem();
      
      deteccao.getPosicaoMovimentos(this);

      imag = deteccao.insereRetangulosNaImagem();
      
      tela.atualizar(getImagemParaExibir());

//      det.getPosicaoMovimentos(this);

//      System.out.println(imag);
    }
  }
  
}
