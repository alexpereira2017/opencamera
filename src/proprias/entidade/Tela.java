/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proprias.entidade;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Alex
 */
public class Tela {
  
  private JLabel vidpanel;
  private Configuracao config;

  public Tela(Configuracao c) {
    this.config = c;
  }
  
  public void definir() {
  
  }
  
  public void ini() {
    vidpanel = new JLabel();
    JFrame jframe = new JFrame(config.getNomeTela());

    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jframe.setContentPane(vidpanel);
    jframe.setSize(config.getTamanhoTelaX(), config.getTamanhoTelaY());
    jframe.setVisible(true);
  }
  
  public void atualizar(ImageIcon image) {
    //System.out.println(image);
    vidpanel.setIcon(image);
    vidpanel.repaint();
  }
  
  public void apresentar() {
  
  }
}
