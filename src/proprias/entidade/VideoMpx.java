
package proprias.entidade;

import proprias.util.DeteccaoMovimentoOriginal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import proprias.util.DeteccaoMovimento;
import proprias.util.MapaDeCalor;


public class VideoMpx extends AbstrataMidia {
  private VideoCapture camera;
  private String link;
  private MapaDeCalor mapa;

  @Override
  public String toString() {
    return "VideoMpx{" + "camera=" + camera + ", link=" + link + ", mapa=" + mapa + '}';
  }

  public DeteccaoMovimento getDeteccao() {
    return deteccao;
  }

  public void setDeteccao(DeteccaoMovimento deteccao) {
    this.deteccao = deteccao;
  }

  public void setMan(MapaDeCalor man) {
    this.mapa = man;
  }

  @Override
  public void setLinkFonte(String link) {
    this.link = link;
  }

  @Override
  public void start() {
    camera = new VideoCapture(link);
  }

  @Override
  public void frameGenerator() {
    camera.read(frame);
//    System.out.println(frame);
  }

  @Override
  public Mat getImagem() {
    imag = frame.clone();
    return imag;
  }

  public Mat getImagem(MapaDeCalor mapa) {
//    imag = mapa.montarTeste(frame.clone());
    imag = mapa.montar(frame.clone());
//    imag = mapa.montarTeste(frame.clone());
    return imag;
  }
  
  public Mat getFrame() {
    return frame;
  }


  @Override
  public void exibir(Tela tela) {
    start();
    Mat imgComRetangulo;

    List<Integer> listaPosicoes = new ArrayList<Integer>();

    Calendar cal = Calendar.getInstance();

    long startTime = 0;

    while(startTime < 60 ) {
//    while(true) {
      startTime++;

      frameGenerator();

      getImagem();     
      
      // Detecção de movimento
      deteccao.getPosicaoMovimentos(this);
//      deteccao.processarCentrosMovimento();
//      deteccao.getMovimentos();
      
      imag = deteccao.insereRetangulosNaImagem();

      tela.atualizar(getImagemParaExibir());
    }
  }


}
