
package proprias.entidade;

/**
 *
 * @author alexd
 */
public class Movimento_serializado {
  private int id;
  private String movimentos;
  private String dt_criacao;
  private String hr_criacao;

  public Movimento_serializado(String movimentos) {
    this.movimentos = movimentos;
  }

  public Movimento_serializado(int id, String movimentos, String dt_criacao, String hr_criacao) {
    this.id = id;
    this.movimentos = movimentos;
    this.dt_criacao = dt_criacao;
    this.hr_criacao = hr_criacao;  
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMovimentos() {
    return movimentos;
  }

  public void setMovimentos(String movimentos) {
    this.movimentos = movimentos;
  }

  public String getDt_criacao() {
    return dt_criacao;
  }

  public void setDt_criacao(String dt_criacao) {
    this.dt_criacao = dt_criacao;
  }

  public String getHr_criacao() {
    return hr_criacao;
  }

  public void setHr_criacao(String hr_criacao) {
    this.hr_criacao = hr_criacao;
  }
  
}
