
package proprias.entidade;

/**
 *
 * @author Alex
 */
public class Movimento {
  private int id;
  private int pos_x;
  private int pos_y;

  public Movimento(int pos_x, int pos_y) {
    this.pos_x = pos_x;
    this.pos_y = pos_y;
  }

  public Movimento() {
    
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Movimento{" + "id=" + id + ", pos_x=" + pos_x + ", pos_y=" + pos_y + '}';
  }

  public Movimento(int id, int pos_x, int pos_y) {
    this.id = id;
    this.pos_x = pos_x;
    this.pos_y = pos_y;
  }

  public int getPos_x() {
    return pos_x;
  }

  public void setPos_x(int pos_x) {
    this.pos_x = pos_x;
  }

  public int getPos_y() {
    return pos_y;
  }

  public void setPos_y(int pos_y) {
    this.pos_y = pos_y;
  }
}
