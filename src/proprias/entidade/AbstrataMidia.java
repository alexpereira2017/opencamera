
package proprias.entidade;

import proprias.util.DeteccaoMovimentoOriginal;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import proprias.util.DeteccaoMovimento;

public abstract class AbstrataMidia implements Midia {
  

  static {
      System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  }
  
  protected Mat frame = new Mat();
  protected Mat imag;
  protected DeteccaoMovimento deteccao;

  public void saveImage(BufferedImage img, String prefixo) {
    long millis = System.currentTimeMillis();
    try {
        File outputfile = new File("D:/Images/comparacao/"+millis+"_"+prefixo+".png");
        System.out.println("DUMP: "+millis+"_"+prefixo+".png");
        ImageIO.write(img, "png", outputfile);
    } catch (Exception e) {
        System.err.println("error");
    }
  
  }
  public void saveImage(BufferedImage img) {
    saveImage(img, "new");
  }

  public BufferedImage mat2bufferedImage(Mat image) {
      MatOfByte bytemat = new MatOfByte();
      Imgcodecs.imencode(".png", image, bytemat);
      byte[] bytes = bytemat.toArray();
      InputStream in = new ByteArrayInputStream(bytes);
      BufferedImage img = null;
      try {
          img = ImageIO.read(in);
      } catch (IOException e) {
          e.printStackTrace();
      }
      return img;
  }

  public ImageIcon getImagemParaExibir() {
    return new ImageIcon(mat2bufferedImage(imag));
  }
  
  public Mat getFrame() {
    return new Mat();
  }

  public DeteccaoMovimento getDeteccao() {
    return deteccao;
  }

  public void setDeteccao(DeteccaoMovimento deteccao) {
    this.deteccao = deteccao;
  }
  
  
  
}
