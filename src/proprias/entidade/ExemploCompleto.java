/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proprias.entidade;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
 
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
 
import org.opencv.core.*;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.highgui.HighGui;
import org.opencv.videoio.VideoCapture;
//import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import static ratiler.JavaCVPrjt01.Mat2bufferedImage;
 
public class ExemploCompleto {
    static long millis;
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    static JLabel vidpanel;
    static Mat imagem = null;
    static Mat frame = new Mat();
    static Mat imgSource;
    static Mat imgCriada;
    static Mat imgCriadaAux;
    static Mat imgPronta = new Mat();
    static Mat outerBox = new Mat();
    static Mat diff_frame = null;
    static Mat tempon_frame = null;
    static int resolucaoNumeroLinhas, resolucaoNumeroColunas;
    static int celulaAltura, celulaLargura;
    static ArrayList<Rect> array = new ArrayList<Rect>();
    static int tamanhoTelaX, tamanhoTelaY;
 
    private static JFrame jframe;
    public static void main(String[] args) {
      
      carregarImagem("D:/Images/Primeira.jpg");
      
      carregarVideo();
      
      tamanhoTelaX = imgSource.width();
      tamanhoTelaY = imgSource.height();

      montarTela();


      
      definirResolucaoDoMapaDeCalor(10, 8);
      
      imgCriada = imgSource.clone();

        
      montarMapaDeCalor();

      
      vidpanel.setIcon(prepararImagemParaApresentar());
 
    }
    
    public static void processar() {
        
    }
    
    public static void montarMapaDeCalor() {
      
      int posX, posY;
      Float alpha;
      posY = 0;
      alpha = 0.8f;

      
      for (int i = 0; i < resolucaoNumeroLinhas; i++) {
        posX = 0;
        for (int j = 0; j < resolucaoNumeroColunas; j++) {
          Rect retangulo = new Rect(posX, posY, celulaLargura, celulaAltura);
          int cor1, cor2;
          cor1 = definirCor();
          cor2 = cor1 == 0 ? 255 : definirCor();
//          Imgproc.rectangle(imgSource, retangulo.br(), retangulo.tl(),new Scalar(200, 50, 0), 1); 
          Imgproc.rectangle(imgSource, retangulo.br(), retangulo.tl(),new Scalar(0, cor1, cor2), -1); // Azul - Verde - Vermelho
          Core.addWeighted(imgCriada, alpha,imgSource , 1 - alpha, 0, imgPronta);
          posX += celulaLargura;
          
        }
        posY += celulaAltura;
        
      }
//      imgPronta = imgSource.clone();
    }
    
    public static int definirCor() {
      System.out.println(Math.random());
      if (Math.random() > 0.5f) {
        return 0;
      } else {
        return 255;
      }
    }
    
    public static void definirResolucaoDoMapaDeCalor(int linhas, int colunas) {
      resolucaoNumeroLinhas = linhas;
      resolucaoNumeroColunas = colunas;
      celulaLargura = Math.floorDiv(tamanhoTelaX , colunas); // 1000 5 = 200
      celulaAltura = Math.floorDiv(tamanhoTelaY, linhas); //600 10 = 60
      System.out.println("celulaAltura: "+celulaAltura);
      System.out.println("celulaLargura: "+celulaLargura);
    }
    
    public static void carregarImagem(String nome) {
      imgSource = Imgcodecs.imread(nome);
    }
    
    public static ImageIcon prepararImagemParaApresentar() {
//      return new ImageIcon(Mat2bufferedImage(imgCriada));    
//      return new ImageIcon(Mat2bufferedImage(imgSource));    
      return new ImageIcon(Mat2bufferedImage(imgPronta));    
    }
    
    public static void montarTela() {
      vidpanel = new JLabel();

      jframe = new JFrame("Mapa de calor");
      jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      jframe.setContentPane(vidpanel);
      jframe.setSize(tamanhoTelaX, tamanhoTelaY);
      jframe.setVisible(true);
    }

    public static void salvarImage(BufferedImage img) {
        
      try {
        File outputfile = new File("D:/Images/new_"+millis+".png");
        System.out.println("DUMP: Imagem salva "+millis);
        ImageIO.write(img, "png", outputfile);
      } catch (Exception e) {
        System.err.println("error");
      }
    }

    public static BufferedImage Mat2bufferedImage(Mat image) {
        MatOfByte bytemat = new MatOfByte();
        Imgcodecs.imencode(".png", image, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage img = null;
        try {
            img = ImageIO.read(in);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return img;
    }
 
    public static ArrayList<Rect> detection_contours(Mat outmat) {
        Mat v = new Mat();
        Mat vv = outmat.clone();
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(vv, contours, v, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
 
        double maxArea = 100;
        int maxAreaIdx = -1;
        Rect r = null;
        ArrayList<Rect> rect_array = new ArrayList<Rect>();
        System.out.println("\n");
        System.out.println("*********************************************");
        System.out.println("DUMP CONTORNOS:");
        System.out.println(millis);
        System.out.println(contours);
        System.out.println("*********************************************\n");
 
        for (int idx = 0; idx < contours.size(); idx++) { 
            Mat contour = contours.get(idx); 
            double contourarea = Imgproc.contourArea(contour); 
            
            if (contourarea > maxArea) {
                // maxArea = contourarea;
                maxAreaIdx = idx;
                r = Imgproc.boundingRect(contours.get(maxAreaIdx));
                rect_array.add(r);
                
                //Adiciona os contornos na imagem
                // dasabilitei pois vou usar somente os retangulos
                //Imgproc.drawContours(imag, contours, maxAreaIdx, new Scalar(0,0, 255));
            }
 
        }

        System.out.println(rect_array);
 
        v.release();
 
        return rect_array;
 
    }

  private static void carregarVideo() {
    VideoCapture camera = new VideoCapture("D:/Fotos/Vídeos/VID_20160827_205347561.mp4");  
  }
}