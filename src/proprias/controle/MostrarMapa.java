
package proprias.controle;

import proprias.entidade.VideoMpx;
import proprias.entidade.Configuracao;
import proprias.entidade.Midia;
import proprias.entidade.Tela;
import proprias.util.FabricaConfiguracaoImagem;
import proprias.util.MapaDeCalor;

/**
 *
 * @author Alex
 */
public class MostrarMapa {
  public static void main(String[] args) {

    Configuracao c = FabricaConfiguracaoImagem.init();
    MapaDeCalor mapaDeCalor;

    Tela tela = new Tela(c);
    tela.ini();

    Midia midia = new VideoMpx();
    midia.setLinkFonte(c.getPathFonteVideo()+c.getNomeFonteVideo());
    midia.start();
    midia.frameGenerator();
    
    mapaDeCalor = new MapaDeCalor(c);
    midia.getImagem(mapaDeCalor);
    
    tela.atualizar(midia.getImagemParaExibir());

  }
  
}
