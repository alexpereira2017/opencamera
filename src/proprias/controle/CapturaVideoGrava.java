package proprias.controle;

import java.util.ArrayList;
import proprias.adapter.Json;
import proprias.dao.MovimentoDao;
import proprias.dao.Movimento_serializadoDao;
import proprias.entidade.Configuracao;
import proprias.entidade.Movimento;
import proprias.entidade.Movimento_serializado;
import proprias.entidade.Tela;
import proprias.entidade.VideoMpx;
import proprias.entidade.VideoMpxSemExibicaoAutomatica;
import proprias.util.Debug;
import proprias.util.DeteccaoMovimento;
import proprias.util.DeteccaoMovimentoOriginal;
import proprias.util.FabricaConfiguracaoImagem;
import proprias.util.MapaDeCalor;

/**
 *
 * @author Alex
 */
public class CapturaVideoGrava {
  private static Configuracao c;
  private static MapaDeCalor m;
  private static Tela tela;
  private static VideoMpxSemExibicaoAutomatica midia;
  
  public static void main(String[] args) {
    c = FabricaConfiguracaoImagem.init();
    
    tela = new Tela(c);
    tela.ini();

    midia = new VideoMpxSemExibicaoAutomatica();
    midia.setLinkFonte(c.getPathFonteVideo()+c.getNomeFonteVideo());
    midia.start();
    midia.frameGenerator();
    
    m = new MapaDeCalor(c);
    midia.getImagem();

    apresentarParaleloGravarSerializado();
  }
  
  public static void apresentarParaleloGravarSerializado() {

    int aux = 0;
    while(aux < 100) {
      aux++;
      int repeticoes = 0;
      midia.setDeteccao(new DeteccaoMovimentoOriginal());
      
      while(repeticoes < 10 ) {

        tela.atualizar(midia.prepararImagemParaExibir());
        repeticoes++;
      }
      DeteccaoMovimento movimentosDetectados = midia.getDeteccao();
      movimentosDetectados.processarCentrosMovimento();
      ArrayList<Movimento> movimentos = movimentosDetectados.getMovimentos();

      Movimento_serializadoDao dao = new Movimento_serializadoDao();

      Json json = new Json();
      String strJson = json.serializar(movimentos);
      
      Movimento_serializado movimento_serializado = new Movimento_serializado(strJson);
      
      Debug.out("Salvando movimento serializado "+strJson);

      dao.salvar(movimento_serializado);
    }
  }

  public static void apresentarParaleloGravarMovimento() {

    int aux = 0;
    while(aux < 100) {
      aux++;
      int repeticoes = 0;
      midia.setDeteccao(new DeteccaoMovimentoOriginal());
      
      while(repeticoes < 10 ) {

        tela.atualizar(midia.prepararImagemParaExibir());
        repeticoes++;
      }
      DeteccaoMovimento movimentosDetectados = midia.getDeteccao();
      movimentosDetectados.processarCentrosMovimento();
      ArrayList<Movimento> movimentos = movimentosDetectados.getMovimentos();

      MovimentoDao dao = new MovimentoDao();

      System.out.println("Salvando "+movimentos.size()+" movimentos");
      for (Movimento mov : movimentos) {
        dao.salvar(mov);
      }
    }
  }

  public static void salvarNoFinalGravarMovimento() {
    
    midia.setDeteccao(new DeteccaoMovimentoOriginal());
//    midia.setDeteccao(new FastDetection());
    
    midia.exibir(tela);

//    tela.atualizar(midia.getImagemParaExibir());

    DeteccaoMovimento movimentosDetectados = midia.getDeteccao();
    movimentosDetectados.processarCentrosMovimento();
    ArrayList<Movimento> movimentos = movimentosDetectados.getMovimentos();
    
    MovimentoDao dao = new MovimentoDao();
    
    System.out.println("Salvando "+movimentos.size()+" movimentos");
    for (Movimento mov : movimentos) {
      dao.salvar(mov);
    }
  }
  
}
