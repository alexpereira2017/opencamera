
package proprias.controle;

import proprias.entidade.Configuracao;
import proprias.entidade.Midia;
import proprias.entidade.Tela;
import proprias.entidade.VideoMpx;
import proprias.entidade.WebCam;
import proprias.util.FabricaConfiguracaoImagem;
import proprias.util.MapaDeCalor;

/**
 *
 * @author alexd
 */
public class MostrarMapaWebCam {
  public static void main(String[] args) {

    Configuracao c = FabricaConfiguracaoImagem.init();
    MapaDeCalor mapaDeCalor;

    Tela tela = new Tela(c);
    tela.ini();

    Midia midia = new WebCam();
    
    midia.start();
    midia.frameGenerator();
    
    mapaDeCalor = new MapaDeCalor(c);
    midia.getImagem(mapaDeCalor);
//    midia.getImagem();
    
//    midia.exibir(tela);
    tela.atualizar(midia.getImagemParaExibir());

  }
}
