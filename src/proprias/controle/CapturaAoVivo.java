
package proprias.controle;

import java.util.ArrayList;
import java.util.Iterator;
import proprias.dao.MovimentoDao;
import proprias.entidade.VideoMpx;
import proprias.entidade.Configuracao;
import proprias.util.DeteccaoMovimentoOriginal;
import proprias.entidade.Midia;
import proprias.entidade.Movimento;
import proprias.entidade.Tela;
import proprias.util.DeteccaoMovimento;
import proprias.util.FabricaConfiguracaoImagem;
import proprias.util.FastDetection;
import proprias.util.MapaDeCalor;

/**
 *
 * @author Alex
 */
public class CapturaAoVivo {
  public static void main(String[] args) {

    Configuracao c = FabricaConfiguracaoImagem.init();
    MapaDeCalor mapa;

    Tela tela = new Tela(c);
    tela.ini();

    VideoMpx midia = new VideoMpx();
//    midia.setLinkFonte(c.getPathFonteVideo()+c.getNomeFonteVideo());
    midia.setLinkFonte("D:/Images/filme_02.mp4");

    midia.start();
    midia.frameGenerator();
    
    mapa = new MapaDeCalor(c);
    midia.getImagem();
    
    midia.setDeteccao(new DeteccaoMovimentoOriginal());
//    midia.setDeteccao(new FastDetection());
    
    midia.exibir(tela);

//    tela.atualizar(midia.getImagemParaExibir());

    DeteccaoMovimento movimentosDetectados = midia.getDeteccao();
    movimentosDetectados.processarCentrosMovimento();
    ArrayList<Movimento> movimentos = movimentosDetectados.getMovimentos();
    
    MovimentoDao dao = new MovimentoDao();
    
    System.out.println("Salvando "+movimentos.size()+" movimentos");
    for (Movimento mov : movimentos) {
      dao.salvar(mov);
    }
  }
}
