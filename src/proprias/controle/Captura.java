
package proprias.controle;

import proprias.entidade.VideoMpx;
import proprias.entidade.Configuracao;
import proprias.entidade.Midia;
import proprias.entidade.Tela;
import proprias.util.FabricaConexao;
import proprias.util.FabricaConfiguracaoImagem;
import proprias.util.MapaDeCalor;

public class Captura {
  
  public static void main(String[] args) {

    Configuracao c = FabricaConfiguracaoImagem.init();
    MapaDeCalor m;

    Tela tela = new Tela(c);
    tela.ini();

    VideoMpx midia = new VideoMpx();
    midia.setLinkFonte("D:/Fotos/Vídeos/VID_20160827_205347561.mp4");
    midia.setMan(new MapaDeCalor(c));
    midia.exibir(tela);

  }
  
}
