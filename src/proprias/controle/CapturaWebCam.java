
package proprias.controle;

import com.sun.corba.se.impl.naming.cosnaming.NamingContextImpl;
import java.util.ArrayList;
import proprias.adapter.Json;
import proprias.dao.MovimentoDao;
import proprias.dao.Movimento_serializadoDao;
import proprias.entidade.AbstrataMidia;
import proprias.entidade.Configuracao;
import proprias.entidade.Midia;
import proprias.entidade.Movimento;
import proprias.entidade.Movimento_serializado;
import proprias.entidade.Tela;
import proprias.entidade.VideoMpx;
import proprias.entidade.WebCam;
import proprias.entidade.WebCamSemExibicaoAutomatica;
import proprias.util.Debug;
import proprias.util.DeteccaoMovimento;
import proprias.util.DeteccaoMovimentoOriginal;
import proprias.util.FabricaConfiguracaoImagem;
import proprias.util.MapaDeCalor;

/**
 *
 * @author alexd
 */
public class CapturaWebCam {
  public static void main(String[] args) {
    Configuracao c = FabricaConfiguracaoImagem.init();

    Tela tela = new Tela(c);
    tela.ini();

//    Midia midia = new WebCam();
    WebCamSemExibicaoAutomatica midia = new WebCamSemExibicaoAutomatica();
    midia.start();
    midia.frameGenerator();
    midia.setDeteccao(new DeteccaoMovimentoOriginal());

    while( true ) {

      int repeticoes = 0;
      while(repeticoes < 50 ) {
        repeticoes++;
        tela.atualizar(midia.prepararImagemParaExibir());
      }

      DeteccaoMovimento movimentosDetectados = midia.getDeteccao();
      movimentosDetectados.processarCentrosMovimento();
      ArrayList<Movimento> movimentos = movimentosDetectados.getMovimentos();
      
      Debug.out("Total de movimentos --> "+movimentos.size());
      
      Json json = new Json();
      String strJson = json.serializar(movimentos);
      
      Movimento_serializado mov = new Movimento_serializado(strJson);
      
      Movimento_serializadoDao dao = new Movimento_serializadoDao();
      dao.salvar(mov);
      
      midia.resetDeteccao();

    }
    

  }
  
  public static void salvarNoFinalGravarMovimento() {

    Configuracao c = FabricaConfiguracaoImagem.init();

    Tela tela = new Tela(c);
    tela.ini();

//    Midia midia = new WebCam();
    WebCam midia = new WebCam();
    midia.start();
    midia.frameGenerator();
    
    midia.setDeteccao(new DeteccaoMovimentoOriginal());
    
    midia.exibir(tela);

//    tela.atualizar(midia.getImagemParaExibir());

    DeteccaoMovimento movimentosDetectados = midia.getDeteccao();
    movimentosDetectados.processarCentrosMovimento();
    ArrayList<Movimento> movimentos = movimentosDetectados.getMovimentos();
    
    MovimentoDao dao = new MovimentoDao();
    
    System.out.println("Salvando "+movimentos.size()+" movimentos");
    for (Movimento mov : movimentos) {
      dao.salvar(mov);
    }
  }
  
  
}
