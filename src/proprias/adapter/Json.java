package proprias.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.util.ArrayList;
import proprias.entidade.Movimento;

/**
 *
 * @author alexd
 */
public class Json {
  private Gson googleJson = new Gson();
  private ArrayList<Movimento> movimentos = new ArrayList<Movimento>();
  
  public String serializar(ArrayList<Movimento> movimentos) {
    Gson gsonBuilder = new GsonBuilder().create();
    String strJson = gsonBuilder.toJson(movimentos);
//    System.out.println(gsonBuilder.toJson(strJson));
    return strJson;
  }
  
  public void arrayPush(String strJson) {
    JsonParser jsonParser = new JsonParser();
    JsonArray arrayFromString = jsonParser.parse(strJson).getAsJsonArray();

    for (int i = 0; i < arrayFromString.size(); i++) {
      movimentos.add(googleJson.fromJson(arrayFromString.get(i), Movimento.class));
    }
  }
  
  public ArrayList<Movimento> getMovimentos() {
    return movimentos;
  }
}
