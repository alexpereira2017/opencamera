/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proprias.util;

import java.util.ArrayList;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import proprias.entidade.AbstrataMidia;
import proprias.entidade.Movimento;

/**
 *
 * @author Alex
 */
public class FastDetection implements DeteccaoMovimento {
  private Mat frame;
  private Mat imag;
  private Mat outerBox;
  private Mat diff_frame;
  private Mat tempon_frame;
  private ArrayList<Rect> listaMovimentosDetectados;

  @Override
  public Movimento calcularCentroRetangulo(Point inicio, Size tamanho) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void debug(String s) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public ArrayList<Rect> detection_contours(Mat outmat) {
    ArrayList<Rect> listaDeteccaoContorno = new ArrayList<Rect>();
    
    return listaDeteccaoContorno;
  }

  @Override
  public ArrayList<Movimento> getMovimentos() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void getPosicaoMovimentos(AbstrataMidia camera) {
    frame = camera.getFrame();
    imag = camera.getImagem();

    outerBox = new Mat(frame.size(), CvType.CV_8UC1);
//        camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox_nova");

    Imgproc.cvtColor(frame, outerBox, Imgproc.COLOR_BGR2GRAY);
//        camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox_1");
    Imgproc.GaussianBlur(outerBox, outerBox, new Size(3, 3), 0);
//        camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox_2");

    if (diff_frame == null) {
        diff_frame = new Mat(outerBox.size(), CvType.CV_8UC1);
//        tempon_frame = new Mat(outerBox.size(), CvType.CV_8UC1);
        tempon_frame = outerBox.clone();
//        diff_frame = outerBox.clone();
    }

    if (diff_frame != null) {
        Core.subtract(outerBox, tempon_frame, diff_frame);
          camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox");
          camera.saveImage(camera.mat2bufferedImage(tempon_frame) , "tempon_frame");
          camera.saveImage(camera.mat2bufferedImage(diff_frame) , "diff_frame_1");
        
        Imgproc.adaptiveThreshold(
                diff_frame, 
                diff_frame, 
                255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C,
                Imgproc.THRESH_BINARY_INV, 5, 2);

        listaMovimentosDetectados = detection_contours(diff_frame);
        camera.saveImage(camera.mat2bufferedImage(diff_frame));
    }
    
    tempon_frame = diff_frame ;
  }

  @Override
  public Mat insereRetangulosNaImagem() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void processarCentrosMovimento() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void setMovimentos(ArrayList<Movimento> movimentos) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
}
