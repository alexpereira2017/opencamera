
package proprias.util;

//import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.ImageIcon;
import jdk.nashorn.internal.codegen.DumpBytecode;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import proprias.entidade.AbstrataMidia;
import proprias.entidade.Movimento;
//import static ratiler.JavaCVPrjt01.detection_contours;

public class DeteccaoMovimentoOriginal implements DeteccaoMovimento {
  ArrayList<Rect> retangulos = new ArrayList<Rect>();
  ArrayList<Rect> listaMovimentosDetectados = new ArrayList<Rect>();
  ArrayList<Rect> listaTodosMovimentosDetectados = new ArrayList<Rect>();
  ArrayList<Movimento> movimentos = new ArrayList<Movimento>();
  private boolean debug = false;

  Mat frame = new Mat();
  Mat imag;
  Mat outerBox = new Mat();
  Mat diff_frame = null;
  Mat tempon_frame = null;
  int cont = 0;
  // Pegar o centro do retangulo
  //Point center_of_rect = (r.br() + r.tl())*0.5;

  @Override
  public ArrayList<Movimento> getMovimentos() {
    return movimentos;
  }

  @Override
  public void setMovimentos(ArrayList<Movimento> movimentos) {
    this.movimentos = movimentos;
  }
  
  @Override
  public void getPosicaoMovimentos(AbstrataMidia camera) {
    frame = camera.getFrame();
    imag = camera.getImagem();

    outerBox = new Mat(frame.size(), CvType.CV_8UC1);
//        camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox_nova");

    Imgproc.cvtColor(frame, outerBox, Imgproc.COLOR_BGR2GRAY);
//        camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox_1");
    Imgproc.GaussianBlur(outerBox, outerBox, new Size(3, 3), 0);
//        camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox_2");

    if (diff_frame == null) {
        diff_frame = new Mat(outerBox.size(), CvType.CV_8UC1);
        tempon_frame = new Mat(outerBox.size(), CvType.CV_8UC1);
        diff_frame = outerBox.clone();
//        tempon_frame = outerBox.clone();
//        System.out.println("Somente uma vez");
//          camera.saveImage(camera.mat2bufferedImage(diff_frame) , "diff_frame");
    }

    if (diff_frame != null) {
        Core.subtract(outerBox, tempon_frame, diff_frame);
//          camera.saveImage(camera.mat2bufferedImage(outerBox) , "outerBox");
//          camera.saveImage(camera.mat2bufferedImage(tempon_frame) , "tempon_frame");
//          camera.saveImage(camera.mat2bufferedImage(diff_frame) , "diff_frame_1");
        
        Imgproc.adaptiveThreshold(diff_frame, diff_frame, 255,
                Imgproc.ADAPTIVE_THRESH_MEAN_C,
                Imgproc.THRESH_BINARY_INV, 5, 2);
        listaMovimentosDetectados = detection_contours(diff_frame);
//        camera.saveImage(camera.mat2bufferedImage(diff_frame));
    }
    
//    camera.saveImage(camera.mat2bufferedImage(tempon_frame) , "tempon_frame_before");
    tempon_frame = outerBox.clone();
//    camera.saveImage(camera.mat2bufferedImage(tempon_frame) , "tempon_frame_after");

  }
  
  @Override
  public Mat insereRetangulosNaImagem() {
        if (listaMovimentosDetectados.size() > 0) {

            Iterator<Rect> it2 = listaMovimentosDetectados.iterator();
            while (it2.hasNext()) {
                Rect obj = it2.next();
//                Core.rectangle(imag, obj.br(), obj.tl(),new Scalar(0, 255, 0), 1);
                Imgproc.rectangle(imag, obj.br(), obj.tl(),new Scalar(0, 255, 0), 1); // Adiciona os retangulos na imagem
            }
        }
        return imag;
  }
  
  public void reset() {
    listaTodosMovimentosDetectados = new ArrayList<Rect>();
    movimentos = new ArrayList<Movimento>();
  }
  
  @Override
  public void processarCentrosMovimento() {
    Movimento m;
    if (listaTodosMovimentosDetectados.size() > 0) {
        Iterator<Rect> it2 = listaTodosMovimentosDetectados.iterator();
        while (it2.hasNext()) {
            Rect obj = it2.next();
            obj.br();
            m = calcularCentroRetangulo(obj.tl(), obj.size());
            movimentos.add(m);
        }
    }
  }
  
  @Override
  public Movimento calcularCentroRetangulo(Point inicio, Size tamanho) {

    double x = inicio.x + (tamanho.width / 2);
    double y = inicio.y + (tamanho.height / 2);
    
    Movimento m = new Movimento((int)x, (int)y);
    
    return m;  
  }
  
  @Override
  public ArrayList<Rect> detection_contours(Mat outmat) {
    Mat v = new Mat();
    Mat vv = outmat.clone();
    List<MatOfPoint> listaContornos = new ArrayList<MatOfPoint>();
    Imgproc.findContours(vv, listaContornos, v, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

//      Gravacao.salvarImage(Gravacao.mat2bufferedImage(vv));

    double maxArea = 100;
    int maxAreaIdx = -1;
    Rect r = null;
    ArrayList<Rect> rect_array = new ArrayList<Rect>();

    debug("*********************************************");
    debug("DUMP CONTORNOS(quantidade de frames): "+listaContornos.size());

    for (int idx = 0; idx < listaContornos.size(); idx++) {
      Mat contour = listaContornos.get(idx);
      double contourarea = Imgproc.contourArea(contour); 
      

      if (contourarea > maxArea) {

          //Debug
          debug("contourarea: "+contourarea);

          maxAreaIdx = idx;
          r = Imgproc.boundingRect(listaContornos.get(maxAreaIdx));
          rect_array.add(r);
          listaTodosMovimentosDetectados.add(r);

      }

    }

    //Debug
    debug("Quantidade rect_array "+rect_array.size());

    v.release();

    return rect_array;

  }
  
  @Override
  public void debug(String s) {
    if (debug) {
      System.out.println(s);
    }
  }
  
}
