package proprias.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Danilo Souza Almeida
 */
public class FabricaConexao {
    
    private static Connection conexao;
//    private static final String URL_CONEXAO = "jdbc:mysql://localhost/sistema_deteccao";
//    private static final String USUARIO = "root";
//    private static final String SENHA = "mylunacom";
    private static final String URL_CONEXAO = "jdbc:mysql://localhost:5501/sistema_deteccao";
    private static final String USUARIO = "root";
    private static final String SENHA = "123456";
  
//    private static final String URL_CONEXAO = "jdbc:mysql://lunacom.com.br/lunac207_geral";
//    private static final String USUARIO = "lunac207_usrgera";
//    private static final String SENHA = "OAkGBlmTqgvU";

    public static Connection getConexao() {
        if(conexao == null){
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conexao = DriverManager.getConnection(URL_CONEXAO, USUARIO, SENHA);
            } catch (SQLException ex) {
                Logger.getLogger(FabricaConexao.class.getName()).log(Level.SEVERE, "Erro no SQL", ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(FabricaConexao.class.getName()).log(Level.SEVERE, "Classe de conexão com banco de dados não encontrada", ex);
            }
        }
        return conexao;
    }
    
    public static void fecharConexao(){
        if(conexao != null){
            try {
                conexao.close();
                conexao = null;
            } catch (SQLException ex) {
                Logger.getLogger(FabricaConexao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
