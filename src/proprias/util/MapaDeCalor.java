/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proprias.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import proprias.adapter.Json;
import proprias.dao.MovimentoDao;
import proprias.dao.Movimento_serializadoDao;
import proprias.entidade.Configuracao;
//import static proprias.entidade.ExemploCompleto.definirCor;
import proprias.entidade.Movimento;
import proprias.entidade.Movimento_serializado;

/**
 *
 * @author Alex
 */
public class MapaDeCalor {
  private Configuracao config;
//  private int celulaAltura, celulaLargura;
  private int qntMovimentoMaiorColuna, qntMovimentoMaiorLinha;
  private Mat imgSource;
  private Mat imgSourceClone;
  private Mat imgPronta;
  private List<Movimento> movimentos = new LinkedList<>();
  private int maiorQuantidadeMovimentos = 0;
  private int param_1;
  private int param_2;
  private int param_3;
  private int cor_r, cor_g, cor_b = 0;
  private int[][] quadrante;

  public MapaDeCalor(Configuracao c) {
    config = c;
    System.out.println(config);
  }
  
  public void buscar() {
//    buscarMovimentos();
    buscarMovimentos_serializados();
  }
  
  public void buscarMovimentos() {
    MovimentoDao dao = new MovimentoDao();
    movimentos = dao.pesquisarMaiorQue(10000,
            config.getTamanhoTelaX(), 
            config.getTamanhoTelaY());  
  }
  
  public void buscarMovimentos_serializados() {
    Json json = new Json();
    Movimento_serializadoDao dao = new Movimento_serializadoDao();
    List<Movimento_serializado> arrMovimentosSerializados = dao.pesquisarMovimentosRecentes(100);
    
    Debug.out("Início movimentos: "+json.getMovimentos().size());

    for (Movimento_serializado movimentoSerializado : arrMovimentosSerializados) {
      json.arrayPush(movimentoSerializado.getMovimentos());
    }

    Debug.out("Total de movimentos: "+json.getMovimentos().size());
    movimentos = json.getMovimentos();
  }
  
  public Mat montar(Mat im) {
    int posX, posY;
    imgSource = im.clone();
    imgSourceClone = im.clone();
    imgPronta = new Mat();
    
    definirPontosDeMovimento();
    posY = 0;
    posX = 0;
    

    for (int y = 0; y < config.getResolucaoNumeroLinhas() + 1; y++) {
      posX = 0;
      for (int x = 0; x < config.getResolucaoNumeroColunas() + 1; x++) {

        // Debug
//        System.out.println("**************************");
//        System.out.println("["+x+"]["+y+"]");
//        System.out.println(posX+" "+posY);

        Rect retangulo = new Rect(posX, posY, config.getCelulaLargura(), config.getCelulaAltura());

        definirCor(quadrante[x][y]);

        Imgproc.rectangle(
                imgSource,
                retangulo.br(),
                retangulo.tl(),
                new Scalar(cor_b, cor_g, cor_r), 
                -1);

        Core.addWeighted(
                imgSource, 
                config.getAlpha(),
                imgSourceClone , 1 - config.getAlpha(), 
                0, 
                imgPronta);
        posX += config.getCelulaLargura();

      }
      posY += config.getCelulaAltura();

    }
    return imgPronta;
  }
  
  public int[][] inicializarMatriz(int qntLinhas, int qntColunas) {
    int[][] matriz = new int[qntColunas + 20][qntLinhas + 20];

    for (int x = 0; x < qntColunas + 1; x++) {
      for (int y = 0; y < qntLinhas + 1; y++) {
//        System.out.println("Inicializando matriz x:"+x+" y:"+y);
        matriz[x][y] = 0;
      }
    }
    return matriz;
  }
  
  public void definirPontosDeMovimento() {
    int qntLinhas, qntColunas, largura, altura, aux;
    qntLinhas = config.getResolucaoNumeroLinhas();
    qntColunas = config.getResolucaoNumeroColunas();
    
    largura = config.getCelulaLargura();
    altura = config.getCelulaAltura();

    quadrante = inicializarMatriz(qntLinhas, qntColunas);
    

   
    buscar();
    
//    System.out.println(qntLinhas+" "+qntColunas);
//    System.out.println(config.getTamanhoTelaX()+" "+config.getTamanhoTelaY());

//    System.out.println(config.getCelulaLargura());
//    System.out.println(config.getCelulaAltura());
    
    for (Movimento mov : movimentos) {
      int x = 1;
      int y = 1;

      aux = largura;
      while (mov.getPos_x() > aux) {
        x++;
        aux = x * largura;
      }
//      System.out.println(mov.getId());
//      System.out.println("Pos x -> "+x+" "+mov.getPos_x()+" "+aux);

      aux = altura;
      while (mov.getPos_y() > aux) {
        y++;
        aux = y * altura;
      }
      
      
      //System.out.println("quadrante["+x+"]["+y+"]");

      quadrante[x][y]++;
      
      if (quadrante[x][y] > maiorQuantidadeMovimentos) {
        maiorQuantidadeMovimentos = quadrante[x][y];
        qntMovimentoMaiorColuna = x;
        qntMovimentoMaiorLinha = y;
      }
    }
    

//    System.out.println("maiorQuantidadeMovimentos: "+maiorQuantidadeMovimentos+" em ["+qntMovimentoMaiorColuna+"]["+qntMovimentoMaiorLinha+"]");
    definirParametrosCores();

  }
  
  private void definirParametrosCores() {
    double div = Math.floor(maiorQuantidadeMovimentos / 3);
    param_1 = (int) div;
    param_2 = (int) (2 * div);
    param_3 = (int) (3 * div);
    
//    System.out.println("Parametros Cores "+param_1);
//    System.out.println("Parametros Cores "+param_2);
//    System.out.println("Parametros Cores "+param_3);
  }
  
//  public Mat montarTeste(Mat im) {
//    int posX, posY;
//    
//    imgSource = im.clone();
//    imgSourceClone = im.clone();
//    imgPronta = new Mat();
//
//    posY = 0;
//
//    for (int x = 0; x < config.getResolucaoNumeroLinhas(); x++) {
//      posX = 0;
//      for (int y = 0; y < config.getResolucaoNumeroColunas(); y++) {
//        Rect retangulo = new Rect(posX, posY, config.getCelulaLargura(), config.getCelulaAltura());
//        int cor1, cor2;
//        cor1 = definirCor(2,3);
//        cor2 = cor1 == 0 ? 255 : definirCor(x, y);
//        
//        System.out.println("["+posX+"]["+posY+"]");
//        System.out.println("["+config.getCelulaLargura()+"]["+config.getCelulaAltura()+"]");
//
//        Imgproc.rectangle(
//                imgSource, 
//                retangulo.br(), 
//                retangulo.tl(),
//                new Scalar(0, cor1, cor2), 
//                -1);
//
//        Core.addWeighted(
//                imgSource, 
//                config.getAlpha(),
//                imgSourceClone , 1 - config.getAlpha(), 
//                0, 
//                imgPronta);
//        posX += config.getCelulaLargura();
//
//      }
//      posY += config.getCelulaAltura();
//
//    }
//    return imgPronta;
//  }
        
    public int definirCor(int x, int y) {   
      if (Math.random() > 0.5f) {
        return 0;
      } else {
        return 255;
      }
    }
    
    public void definirCor(int ref) {

//        Verde    = 0  , 255, 0
//        Amarelo  = 255, 255, 0
//        Vermelho = 255,   0, 0
      
      if (ref > param_2) {
        Debug.out("Vermelho "+ref);
        cor_r = 255;
        cor_g = 0;
        cor_b = 0;
      } else if (ref > param_1) {
        Debug.out("Amarelo "+ref);
        cor_r = 255;
        cor_g = 255;
        cor_b = 0;      
      } else if (ref > 3 ){
        Debug.out("Verde "+ref);
        cor_r = 0;
        cor_g = 255;
        cor_b = 0;      
      } else {
        Debug.out("Nada "+ref);
        cor_r = 0;
        cor_g = 0;
        cor_b = 0;
      }

    }
  
}
