/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proprias.util;

import java.util.ArrayList;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import proprias.entidade.AbstrataMidia;
import proprias.entidade.Movimento;

/**
 *
 * @author Alex
 */
public interface DeteccaoMovimento {

  Movimento calcularCentroRetangulo(Point inicio, Size tamanho);

  void debug(String s);

  ArrayList<Rect> detection_contours(Mat outmat);

  ArrayList<Movimento> getMovimentos();

  void getPosicaoMovimentos(AbstrataMidia camera);

  Mat insereRetangulosNaImagem();

  void processarCentrosMovimento();

  void setMovimentos(ArrayList<Movimento> movimentos);

  void reset();
  
}
