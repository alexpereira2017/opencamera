
package proprias.util;

import proprias.entidade.Configuracao;

public class FabricaConfiguracaoImagem {

  public static Configuracao init() {
    Configuracao conf = new Configuracao();
    
    conf.setNomeTela("Captura de movimento por imagem");
    
    conf.setPathFonteVideo("D:/Images/");
    conf.setNomeFonteVideo("filme_02.mp4");
    conf.setTamanhoTelaX(780);
    conf.setTamanhoTelaY(620);
    conf.setAlpha(0.2f);
    conf.setResolucaoNumeroLinhas(30);
    conf.setResolucaoNumeroColunas(30);
    int celulaLargura = (int) conf.getTamanhoTelaX() / conf.getResolucaoNumeroColunas();
    int celulaAltura = (int) conf.getTamanhoTelaY() / conf.getResolucaoNumeroLinhas();
    conf.setCelulaLargura(celulaLargura);
    conf.setCelulaAltura(celulaAltura);
    
    return conf;
  }
  
}
