
package proprias.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import javax.imageio.ImageIO;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author Alex
 */
public class Gravacao {
  
  static long millis;
  
  public static void salvarImage(BufferedImage img) {
    
    Calendar calendar = Calendar.getInstance();
    millis = calendar.getTimeInMillis();
    try {
      File outputfile = new File("D:/Images/new_"+millis+".png");
      System.out.println("DUMP: Imagem salva "+millis);
      ImageIO.write(img, "png", outputfile);
    } catch (Exception e) {
      System.err.println("error");
    }
  }
  
  public static BufferedImage mat2bufferedImage(Mat image) {
      MatOfByte bytemat = new MatOfByte();
      Imgcodecs.imencode(".png", image, bytemat);
      byte[] bytes = bytemat.toArray();
      InputStream in = new ByteArrayInputStream(bytes);
      BufferedImage img = null;
      try {
          img = ImageIO.read(in);
      } catch (IOException e) {
          e.printStackTrace();
      }
      return img;
  }
  
}
