create DATABASE sistema_deteccao;

CREATE TABLE movimento (
  id int(20) NOT NULL AUTO_INCREMENT,
  pos_x int(5) NOT NULL,
  pos_y int(5) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE movimento_serializado (
  id int(20) NOT NULL AUTO_INCREMENT,
  movimentos text,
  dt_criacao date DEFAULT NULL,
  hr_criacao time DEFAULT NULL,
  PRIMARY KEY (id)
);

SELECT * from movimento order by id desc ;

INSERT INTO movimento_serializado (movimentos, dt_criacao, hr_criacao)
VALUES ('[{"id":0,"pos_x":50,"pos_y":200},{"id":0,"pos_x":100,"pos_y":80},{"id":0,"pos_x":700,"pos_y":50},{"id":0,"pos_x":700,"pos_y":50},{"id":0,"pos_x":700,"pos_y":50},{"id":0,"pos_x":700,"pos_y":50}]', curdate(), curtime());

    SELECT * FROM sistema_deteccao.movimento_serializado ORDER BY id DESC ;
    
    SELECT (curtime() - 6000), hr_criacao - 0, ms.*
    FROM movimento_serializado ms
    WHERE dt_criacao = CURDATE()
    AND (hr_criacao - 0) > (CURTIME() - 5000)
    ORDER BY id DESC ;