# Opencamera Mapa de calor

O software realiza a captura de imagens em um ambiente através de uma webcam instalada no computador. Durante a captação é realizada a captura dos movimentos e salvos em banco de dados mysql. Depois, é possível apresentar o mapa de calor dos pontos com maior movimento.

## Getting Started

Estas são as principais funcionalidades que o sistema realiza.

* O arquivo Sql\OpenCamera.sql possui o script para criação do banco de dados;
* O aqruivo FabricaConexao.java possui os dados para conexão com o banco;
* O arquivo FabricaConfiguracaoImagem.java possui dados de configuração para rodar o script;
* Executar os arquivos do pacote controle para:
	* Capturar imagem e gravar a partir de um vídeo em formado MP3
	* Capturar imagem da webcam e gravar
	* Mostrar o mapa de calor dos movimentos

## Built With

* Java
* OpenCV
* Mysql
* Gson

## Autor

* **Alex L Pereira**

## Licença

This project is licensed under the MIT License 